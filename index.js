const dayjs = require("dayjs");
const axios = require("axios");

const poolAddress = "0x59f39d449bd185e3bd8d8473f160b1d524a7b927";

const getBlock = async (time) => {
    const query = `
        query ($from: BigInt, $to: BigInt) {
            blocks(
                first: 1
                orderBy: timestamp
                orderDirection: desc
                where: { timestamp_gt: $from, timestamp_lt: $to }
            ) {
                number
            }
        }
    `;
    const result = await axios.post(
        "https://api.thegraph.com/subgraphs/name/pancakeswap/blocks",
        {
            query,
            variables: { from: time, to: time + 600 },
        },
        {
            headers: {
                "Content-Type": "application/json",
            },
        }
    );

    return Number(result.data.data.blocks[0].number);
};

const getPools = async (oneWeekAgoBlock) => {
    const query = `
        query ($oneWeekAgoBlock: Int!, $pools: [ID!]) {
            now: pairs(where: { id_in: $pools }) {
                reserveUSD
                volumeUSD
            }
            oneWeekAgo: pairs(
                where: { id_in: $pools }
                block: { number: $oneWeekAgoBlock }
            ) {
                volumeUSD
            }
        }
    `;
    const result = await axios.post(
        "https://bsc.streamingfast.io/subgraphs/name/pancakeswap/exchange-v2",
        {
            query,
            variables: {
                oneWeekAgoBlock,
                pools: [poolAddress],
            },
        },
        {
            headers: {
                "Content-Type": "application/json",
                origin: "https://pancakeswap.finance",
                referer: "https://pancakeswap.finance/",
            },
        }
    );

    return result.data.data;
};

(async () => {
    const LP_HOLDERS_FEE = 0.0017;
    const WEEKS_IN_YEAR = 52.1429;

    const oneWeekAgo = dayjs().subtract(1, "week").unix();
    const oneWeekAgoBlock = await getBlock(oneWeekAgo);
    const pools = await getPools(oneWeekAgoBlock);

    const liquidityUSD = Number(pools.now[0].reserveUSD);
    const volumeUSDWeek =
        Number(pools.now[0].volumeUSD) - Number(pools.oneWeekAgo[0].volumeUSD);

    const lpApr7d =
        (volumeUSDWeek * LP_HOLDERS_FEE * WEEKS_IN_YEAR * 100) / liquidityUSD;

    console.log(lpApr7d);
})();
